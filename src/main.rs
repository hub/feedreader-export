/*
 * Author: Hubert Figuière <hub@figuiere.net>
 * License: GPL-3.0+
 */

use serde_derive::Deserialize;
use std::io::Write;
use std::path::{Path, PathBuf};
use clap::Parser;

#[derive(Parser, Debug, Deserialize)]
struct Cli {
    input: PathBuf,
    /// Output file
    #[arg(short, long)]
    output: Option<PathBuf>,
    /// Debug
    #[arg(short, long)]
    debug: bool,
}

#[derive(Debug)]
enum OutlineType {
    Top,
    Category(Category),
    Feed(Feed),
}

#[derive(Debug)]
struct Feed {
    xml_url: String,
    description: String,
    html_url: String,
}

#[derive(Debug)]
struct Category {
    id: String,
    text: String,
    parent: Option<String>,
}

#[derive(Debug)]
struct OutlineItem {
    outline_type: OutlineType,
    items: Vec<OutlineItem>,
}

impl OutlineItem {
    fn to_opml(&self) -> String {
        match self.outline_type {
            OutlineType::Top => {
                let mut opml = String::new();
                opml.push_str("<head>\n");
                opml.push_str("<title>FeedReader Subscriptions</title>\n");
                opml.push_str("</head>\n");
                opml.push_str("<body>\n");
                for item in &self.items {
                    opml.push_str(&item.to_opml());
                }
                opml.push_str("</body>\n");

                opml
            }
            OutlineType::Category(ref c) => {
                let mut opml = format!(
                    "<outline text=\"{}\">\n",
                    xml::escape::escape_str_attribute(&c.text)
                );
                for item in &self.items {
                    opml.push_str(&item.to_opml());
                }
                opml.push_str("</outline>\n");
                opml
            }
            OutlineType::Feed(ref f) => {
                format!(
                    "<outline type=\"rss\" text=\"{}\" xmlUrl=\"{}\" description=\"{}\" htmlUrl=\"{}\" />\n",
                    xml::escape::escape_str_attribute(&f.description),
                    xml::escape::escape_str_attribute(&f.xml_url),
                    xml::escape::escape_str_attribute(&f.description),
                    xml::escape::escape_str_attribute(&f.html_url)
                        )
            }
        }
    }

    fn write_opml<F>(&self, mut out: F) -> std::io::Result<()>
    where
        F: Write,
    {
        out.write_all(b"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")?;
        out.write_all(b"<opml version=\"1.0\">\n")?;
        out.write_all(self.to_opml().as_bytes())?;
        out.write_all(b"</opml>")?;

        Ok(())
    }
}

fn load_outline(path: &Path) -> Option<OutlineItem> {
    let conn = rusqlite::Connection::open(path).unwrap();

    let items = get_items_for_parent(&conn, None);
    let top_level = OutlineItem {
        outline_type: OutlineType::Top,
        items,
    };

    Some(top_level)
}

fn get_items_for_parent(conn: &rusqlite::Connection, parent: Option<&str>) -> Vec<OutlineItem> {
    let mut stmt = conn
        .prepare("select categorieID, title, Parent from categories where Parent = :parent")
        .unwrap();
    let mut cat_items: Vec<OutlineItem> = stmt
        .query_map(&[&parent.unwrap_or("-2")], |row| {
            let id: String = row.get(0);
            let items = get_items_for_parent(conn, Some(&id));
            OutlineItem {
                outline_type: OutlineType::Category(Category {
                    id,
                    text: row.get(1),
                    parent: row.get(2),
                }),
                items,
            }
        })
        .unwrap()
        .map(|i| i.unwrap())
        .collect();

    let query = format!(
        "select name, url, xmlUrl from feeds where category_id = :id {}",
        if parent.is_none() {
            "or category_id not in (select categorieID from categories)"
        } else {
            ""
        }
    );
    let mut stmt = conn.prepare(&query).unwrap();
    let feed_items: Vec<OutlineItem> = stmt
        .query_map(&[parent.as_ref().unwrap_or(&"-2")], |row| OutlineItem {
            outline_type: OutlineType::Feed(Feed {
                description: row.get(0),
                html_url: row.get(1),
                xml_url: row.get(2),
            }),
            items: vec![],
        })
        .unwrap()
        .map(|i| i.unwrap())
        .collect();
    cat_items.extend(feed_items);
    cat_items
}

fn main() -> std::io::Result<()> {
    let args = Cli::parse();

    let opml: OutlineItem = load_outline(&args.input).expect("Failed parsing");

    if let Some(output) = args.output {
        opml.write_opml(std::fs::File::create(&output).unwrap())?;
    } else {
        let stdout = std::io::stdout();
        opml.write_opml(stdout.lock())?;
    }

    Ok(())
}
