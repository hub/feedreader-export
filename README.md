A tool to convert Feedreader feeds to OPML. I wrote this because there
was no way to export my feed list from Feedreader.

You need Rust to build this:

To export your Feedreader feeds to OPML, just do:

```
cargo run -- --output feeds.opml ~/.var/app/org.gnome.FeedReader/data/feedreader/data/feedreader-7.db
```

Hubert Figuière
Juy 3rd 2020

License: GPL-3.0+
See file COPYING.
